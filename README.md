# README #

Coursework Group 28

### Research Question ###

Does the number of games a developer releases within a year impact the sales they produce? 
Do these sales reflect the user ratings? 

### Group Members ###

* Paige Draper
* Victoria Ali
* Adinarayana Reddy Goprireddy
* Akash Prakash
* Santosh Kumar Kukkadapu

## Trello Board ##

https://trello.com/invite/b/nlwDSPWD/5c50334a99ccb110b433366591abd8b3/7com1079-team-research-group-28 
